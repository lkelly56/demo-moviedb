document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();

    if (location.pathname.search("fiche-film.html") > 0) {

        var params = (new URL(document.location)).searchParams;

        //console.log( params.get("id") );

        connexion.requeteInfosFilm(params.get("id"));

    }

    else {

        connexion.requeteFilmPopulaire();

    }

});


class MovieDB {

    constructor() {
        console.log("Parfait 2");

        this.APIKey = "3066c48f994bbede257f2e48690a2b52";

        this.lang = "fr-FR";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire() {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            console.log(data);

            this.afficheFilmPopulaire(data);

        }

    }


    afficheFilmPopulaire(data) {

        for (var i = 0; i < this.totalFilm; i++) {

            var unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            //--------------------------------------------------------------------------

            if (data[i].overview == "") {

                unArticle.querySelector(".description").innerText = "Sans description";

            }
            else {
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            //--------------------------------------------------------------------------


            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            //--------------------------------------------------------------------------


            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path)


            //--------------------------------------------------------------------------

            document.querySelector(".liste-films").appendChild(unArticle);

            // console.log(data[5].title);

        }
    }


    requeteInfosFilm(movieId) {


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfosFilm.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");

        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

        //console.log(movieId);


    }


    retourInfosFilm(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheInfosFilm(data);

        }

    }


    afficheInfosFilm(data) {


        var unArticle = document.querySelector(".fiche-film");

        unArticle.querySelector("h1").innerText = data.title;

        //--------------------------------------------------------------------------

        if (data.overview == "") {

            unArticle.querySelector(".description").innerText = "Sans description";

        }
        else {
            unArticle.querySelector(".description").innerText = data.overview;
        }


        //--------------------------------------------------------------------------


        unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data.poster_path)

        this.requeteActeurs(data.id);
    }


    requeteActeurs(movieId) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeurs.bind(this));

        // xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?api_key=" + this.APIKey);

        xhr.send();

    }


    retourActeurs (e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheActeurs(data);

        }

    }


    afficheActeurs(data) {

        for (var i = 0; i < 5; i++) {

            // TODO Faire le template des acteurs

        }

    }
}