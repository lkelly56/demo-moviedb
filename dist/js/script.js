document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();

    if (location.pathname.search("fiche-film.html") > 0) {

        var params = (new URL(document.location)).searchParams;

        //console.log( params.get("id") );

        connexion.requeteInfosFilm(params.get("id"));

    }

    else {

        connexion.requeteFilmPopulaire();

    }

});


class MovieDB {

    constructor() {
        console.log("Parfait 2");

        this.APIKey = "3066c48f994bbede257f2e48690a2b52";

        this.lang = "fr-FR";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire() {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            console.log(data);

            this.afficheFilmPopulaire(data);

        }

    }


    afficheFilmPopulaire(data) {

        for (var i = 0; i < this.totalFilm; i++) {

            var unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            //--------------------------------------------------------------------------

            if (data[i].overview == "") {

                unArticle.querySelector(".description").innerText = "Sans description";

            }
            else {
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            //--------------------------------------------------------------------------


            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            //--------------------------------------------------------------------------


            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path)


            //--------------------------------------------------------------------------

            document.querySelector(".liste-films").appendChild(unArticle);

            // console.log(data[5].title);

        }
    }


    requeteInfosFilm(movieId) {


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfosFilm.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");

        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

        //console.log(movieId);


    }


    retourInfosFilm(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheInfosFilm(data);

        }

    }


    afficheInfosFilm(data) {


        var unArticle = document.querySelector(".fiche-film");

        unArticle.querySelector("h1").innerText = data.title;

        //--------------------------------------------------------------------------

        if (data.overview == "") {

            unArticle.querySelector(".description").innerText = "Sans description";

        }
        else {
            unArticle.querySelector(".description").innerText = data.overview;
        }


        //--------------------------------------------------------------------------


        unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data.poster_path)

        this.requeteActeurs(data.id);
    }


    requeteActeurs(movieId) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeurs.bind(this));

        // xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?api_key=" + this.APIKey);

        xhr.send();

    }


    retourActeurs (e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheActeurs(data);

        }

    }


    afficheActeurs(data) {

        for (var i = 0; i < 5; i++) {

            // TODO Faire le template des acteurs

        }

    }
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKCkge1xuXG5cbiAgICB2YXIgY29ubmV4aW9uID0gbmV3IE1vdmllREIoKTtcblxuICAgIGlmIChsb2NhdGlvbi5wYXRobmFtZS5zZWFyY2goXCJmaWNoZS1maWxtLmh0bWxcIikgPiAwKSB7XG5cbiAgICAgICAgdmFyIHBhcmFtcyA9IChuZXcgVVJMKGRvY3VtZW50LmxvY2F0aW9uKSkuc2VhcmNoUGFyYW1zO1xuXG4gICAgICAgIC8vY29uc29sZS5sb2coIHBhcmFtcy5nZXQoXCJpZFwiKSApO1xuXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXRlSW5mb3NGaWxtKHBhcmFtcy5nZXQoXCJpZFwiKSk7XG5cbiAgICB9XG5cbiAgICBlbHNlIHtcblxuICAgICAgICBjb25uZXhpb24ucmVxdWV0ZUZpbG1Qb3B1bGFpcmUoKTtcblxuICAgIH1cblxufSk7XG5cblxuY2xhc3MgTW92aWVEQiB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJQYXJmYWl0IDJcIik7XG5cbiAgICAgICAgdGhpcy5BUElLZXkgPSBcIjMwNjZjNDhmOTk0YmJlZGUyNTdmMmU0ODY5MGEyYjUyXCI7XG5cbiAgICAgICAgdGhpcy5sYW5nID0gXCJmci1GUlwiO1xuXG4gICAgICAgIHRoaXMuYmFzZVVSTCA9IFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9cIjtcblxuICAgICAgICB0aGlzLmltZ1BhdGggPSBcImh0dHBzOi8vaW1hZ2UudG1kYi5vcmcvdC9wL1wiO1xuXG4gICAgICAgIHRoaXMubGFyZ2V1ckFmZmljaGUgPSBbXCI5MlwiLCBcIjE1NFwiLCBcIjE4NVwiLCBcIjM0MlwiLCBcIjUwMFwiLCBcIjc4MFwiXTtcblxuICAgICAgICB0aGlzLmxhcmdldXJUZXRlQWZmaWNoZSA9IFtcIjQ1XCIsIFwiMTg1XCJdO1xuXG4gICAgICAgIHRoaXMudG90YWxGaWxtID0gODtcblxuICAgICAgICB0aGlzLnRvdGFsQWN0ZXVyID0gNjtcbiAgICB9XG5cbiAgICByZXF1ZXRlRmlsbVBvcHVsYWlyZSgpIHtcblxuXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91ckZpbG1Qb3B1bGFpcmUuYmluZCh0aGlzKSk7XG5cbiAgICAgICAgLy94aHIub3BlbihcIkdFVFwiLCBcImh0dHBzOi8vYXBpLnRoZW1vdmllZGIub3JnLzMvbW92aWUvcG9wdWxhcj9wYWdlPTEmbGFuZ3VhZ2U9ZW4tVVMmYXBpX2tleT0lM0MlM0NhcGlfa2V5JTNFJTNFXCIpO1xuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgKyBcIm1vdmllL3BvcHVsYXI/cGFnZT0xJmxhbmd1YWdlPVwiICsgdGhpcy5sYW5nICsgXCImYXBpX2tleT1cIiArIHRoaXMuQVBJS2V5KTtcblxuICAgICAgICB4aHIuc2VuZCgpO1xuICAgIH1cblxuXG4gICAgcmV0b3VyRmlsbVBvcHVsYWlyZShlKSB7XG5cbiAgICAgICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcblxuICAgICAgICB2YXIgZGF0YTtcblxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XG5cbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpLnJlc3VsdHM7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuXG4gICAgICAgICAgICB0aGlzLmFmZmljaGVGaWxtUG9wdWxhaXJlKGRhdGEpO1xuXG4gICAgICAgIH1cblxuICAgIH1cblxuXG4gICAgYWZmaWNoZUZpbG1Qb3B1bGFpcmUoZGF0YSkge1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy50b3RhbEZpbG07IGkrKykge1xuXG4gICAgICAgICAgICB2YXIgdW5BcnRpY2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT4uZmlsbVwiKS5jbG9uZU5vZGUodHJ1ZSk7XG5cbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaDFcIikuaW5uZXJUZXh0ID0gZGF0YVtpXS50aXRsZTtcblxuICAgICAgICAgICAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gICAgICAgICAgICBpZiAoZGF0YVtpXS5vdmVydmlldyA9PSBcIlwiKSB7XG5cbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBcIlNhbnMgZGVzY3JpcHRpb25cIjtcblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCIuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gZGF0YVtpXS5vdmVydmlldztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiYVwiKS5zZXRBdHRyaWJ1dGUoXCJocmVmXCIsIFwiZmljaGUtZmlsbS5odG1sP2lkPVwiICsgZGF0YVtpXS5pZCk7XG5cblxuICAgICAgICAgICAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaW1nXCIpLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcInc1MDBcIiArIGRhdGFbaV0ucG9zdGVyX3BhdGgpXG5cblxuICAgICAgICAgICAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxpc3RlLWZpbG1zXCIpLmFwcGVuZENoaWxkKHVuQXJ0aWNsZSk7XG5cbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGRhdGFbNV0udGl0bGUpO1xuXG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIHJlcXVldGVJbmZvc0ZpbG0obW92aWVJZCkge1xuXG5cbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91ckluZm9zRmlsbS5iaW5kKHRoaXMpKTtcblxuICAgICAgICAvL3hoci5vcGVuKFwiR0VUXCIsIFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9tb3ZpZS8lN0Jtb3ZpZV9pZCU3RD9sYW5ndWFnZT1lbi1VUyZhcGlfa2V5PSUzQyUzQ2FwaV9rZXklM0UlM0VcIik7XG5cbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICsgXCJtb3ZpZS9cIiArIG1vdmllSWQgKyBcIj9sYW5ndWFnZT1cIiArIHRoaXMubGFuZyArIFwiJmFwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XG5cbiAgICAgICAgeGhyLnNlbmQoKTtcblxuICAgICAgICAvL2NvbnNvbGUubG9nKG1vdmllSWQpO1xuXG5cbiAgICB9XG5cblxuICAgIHJldG91ckluZm9zRmlsbShlKSB7XG5cbiAgICAgICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcblxuICAgICAgICB2YXIgZGF0YTtcblxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XG5cbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpO1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlSW5mb3NGaWxtKGRhdGEpO1xuXG4gICAgICAgIH1cblxuICAgIH1cblxuXG4gICAgYWZmaWNoZUluZm9zRmlsbShkYXRhKSB7XG5cblxuICAgICAgICB2YXIgdW5BcnRpY2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5maWNoZS1maWxtXCIpO1xuXG4gICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaDFcIikuaW5uZXJUZXh0ID0gZGF0YS50aXRsZTtcblxuICAgICAgICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbiAgICAgICAgaWYgKGRhdGEub3ZlcnZpZXcgPT0gXCJcIikge1xuXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBcIlNhbnMgZGVzY3JpcHRpb25cIjtcblxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCIuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gZGF0YS5vdmVydmlldztcbiAgICAgICAgfVxuXG5cbiAgICAgICAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5cbiAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwidzUwMFwiICsgZGF0YS5wb3N0ZXJfcGF0aClcblxuICAgICAgICB0aGlzLnJlcXVldGVBY3RldXJzKGRhdGEuaWQpO1xuICAgIH1cblxuXG4gICAgcmVxdWV0ZUFjdGV1cnMobW92aWVJZCkge1xuXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJBY3RldXJzLmJpbmQodGhpcykpO1xuXG4gICAgICAgIC8vIHhoci5vcGVuKFwiR0VUXCIsIFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9tb3ZpZS8lN0Jtb3ZpZV9pZCU3RC9jcmVkaXRzP2FwaV9rZXk9JTNDJTNDYXBpX2tleSUzRSUzRVwiKTtcbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICsgXCJtb3ZpZS9cIiArIG1vdmllSWQgKyBcIi9jcmVkaXRzP2FwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XG5cbiAgICAgICAgeGhyLnNlbmQoKTtcblxuICAgIH1cblxuXG4gICAgcmV0b3VyQWN0ZXVycyAoZSkge1xuXG4gICAgICAgIHZhciB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG5cbiAgICAgICAgdmFyIGRhdGE7XG5cbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xuXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KTtcblxuICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZUFjdGV1cnMoZGF0YSk7XG5cbiAgICAgICAgfVxuXG4gICAgfVxuXG5cbiAgICBhZmZpY2hlQWN0ZXVycyhkYXRhKSB7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCA1OyBpKyspIHtcblxuICAgICAgICAgICAgLy8gVE9ETyBGYWlyZSBsZSB0ZW1wbGF0ZSBkZXMgYWN0ZXVyc1xuXG4gICAgICAgIH1cblxuICAgIH1cbn0iXSwiZmlsZSI6InNjcmlwdC5qcyJ9
